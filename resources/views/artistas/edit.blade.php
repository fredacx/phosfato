<form role="form" id="form_artistas" action="{{route('artistas.update', $artista)}}" method="post" >
{{csrf_field()}}
@include('artistas._form')
<div class="row">
    <div class="ml-3 mt-3">
		<button type="submit">&nbsp;</i> Salvar</button>
    </div>
</div>    
</form>

<a href="{{ route('artistas.index') }}">Voltar</a>
<h1>Artistas</h1>

@if (Session::has('success'))
	<p>{{ Session::get('success') }}</p>
@endif
@if (Session::has('error'))
	<strong>Erro: </strong><p>{{ Session::get('error') }}</p>
@endif

<p><a href="{{ route('artistas.create') }}">Adicionar</a></p>

<form role="form" id="form_filtro" action="{{route('artistas.index')}}" method="get" >
    <label for="nome">Nome <span class="text-danger">*</span></label>
    <input class="form-control" type="text" id="nome" maxlength="150" name="nome" value="{{Request::get('nome')}}" />

    <label for="tipo">Gênero <span class="text-danger">*</span></label>
	<select class="form-control" name="genero" id="genero">
		<option value="" selected="selected"></option>
		@foreach($generos as $genero)
            @if(Request::get('genero') && Request::get('genero') == $genero)
                <option value="{{$genero}}" selected="selected">{!! $genero !!}</option>
            @else
                <option value="{{$genero}}">{!! $genero !!}</option>
            @endif
        @endforeach
    </select> 
    <button type="submit">Filtrar</button>       
</form>



<table>
	<thead>
		<tr>
			<th>Nome</th>
			<th>Gênero</th>
			<th>Ações</th>
		</tr>
	</thead>
	<tbody>
		@foreach($artistas as $artista)
			<tr>
				<td>{{ $artista->nome }}</td>
				<td>{{ $artista->genero }}</td>
				<td>
					<a href="{{ route('artistas.show', $artista) }}">Ver</a>
					<a href="{{ route('artistas.edit', $artista) }}">Editar</a>
					<a href="{{ route('artistas.destroy', $artista) }}">Remover</a>
					<a href="{{ route('musicas.musicasArtista', $artista) }}">Músicas</a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
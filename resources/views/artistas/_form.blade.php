<div class="row mb-4 filters">
    <div class="col-md-4 col-12">
        <label for="nome">Nome <span class="text-danger">*</span></label>
        <input class="form-control" type="text" id="nome" maxlength="150" name="nome" value="{{@old('nome')??$artista->nome??''}}" required />
    </div>

    <div class="col-md-4 col-12">
        <label for="tipo">Gênero <span class="text-danger">*</span></label>
		<select class="form-control" name="genero" id="genero" required>

					
			@foreach($generos as $genero)
                @if(isset($artista) && $artista->genero == $genero)
                    <option value="{{$genero}}" selected="selected">{!! $genero !!}</option>
                @else
                    <option value="{{$genero}}">{!! $genero !!}</option>
                @endif
            @endforeach


        </select>        
    </div>
</div>
<h1>Artistas</h1>

<table>
	<thead>
		<tr>
			<th>Nome</th>
			<th>Gênero</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{ $artista->nome }}</td>
			<td>{{ $artista->genero }}</td>
		</tr>
	</tbody>
</table>

<a href="{{ route('artistas.index') }}">Voltar</a>
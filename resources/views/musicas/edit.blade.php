<form role="form" id="form_musicas" action="{{route('musicas.update', $musica)}}" method="post" >
{{csrf_field()}}
@include('musicas._form')
<div class="row">
    <div class="ml-3 mt-3">
		<button type="submit">&nbsp;</i> Salvar</button>
    </div>
</div>    
</form>

<a href="{{ route('musicas.index') }}">Voltar</a>
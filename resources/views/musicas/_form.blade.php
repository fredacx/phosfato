<div class="row mb-4 filters">
    <div class="col-md-4 col-12">
        <label for="nome">Nome <span class="text-danger">*</span></label>
        <input class="form-control" type="text" id="nome" maxlength="150" name="nome" value="{{@old('nome')??$musica->nome??''}}" required />
    </div>

    <div class="col-md-4 col-12">
        <label for="tipo">Artista <span class="text-danger">*</span></label>
		<select class="form-control" name="artistas_id" id="artistas_id" required>

					
			@foreach($artistas as $artista)
                @if(isset($musica) && $musica->artistas_id == $artista->id)
                    <option value="{{$artista->id}}" selected="selected">{!! $artista->nome !!}</option>
                @else
                    <option value="{{$artista->id}}">{!! $artista->nome !!}</option>
                @endif
            @endforeach


        </select>        
    </div>

    <div class="col-md-4 col-12">
        <label for="duracao">Duração <span class="text-danger">*</span></label>
        <input class="form-control" type="text" id="duracao" maxlength="150" name="duracao" placeholder="00:00:00" value="{{@old('nome')??$musica->duracao??''}}" required />
    </div>

</div>
<h1>Músicas</h1>

@if (Session::has('success'))
	<p>{{ Session::get('success') }}</p>
@endif
@if (Session::has('error'))
	<strong>Erro: </strong><p>{{ Session::get('error') }}</p>
@endif

<p><a href="{{ route('musicas.create') }}">Adicionar</a></p>


<table>
	<thead>
		<tr>
			<th>Nome</th>
			<th>Artista</th>
			<th>Duração</th>
			<th>Ações</th>
		</tr>
	</thead>
	<tbody>
		@foreach($musicas as $musica)
			<tr>
				<td>{{ $musica->nome }}</td>
				<td>{{ $musica->artista }}</td>
				<td>{{ $musica->duracao }}</td>
				<td>
					<a href="{{ route('musicas.show', $musica) }}">Ver</a>
					<a href="{{ route('musicas.edit', $musica) }}">Editar</a>
					<a href="{{ route('musicas.destroy', $musica) }}">Remover</a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

<p><a href="{{ route('artistas.index') }}">Voltar</a></p>
<h1>Artistas</h1>

<table>
	<thead>
		<tr>
			<th>Nome</th>
			<th>Artista</th>
			<th>Duração</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{ $musica->nome }}</td>
			<td>{{ $musica->artista }}</td>
			<td>{{ $musica->duracao }}</td>
		</tr>
	</tbody>
</table>

<a href="{{ route('musicas.index') }}">Voltar</a>
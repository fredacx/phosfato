<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Artistas
Route::name('artistas.')->group(function () {

	Route::post('artistas/store', [
	    'as'   => 'store',
	    'uses' => 'ArtistasController@store',
	]);
	Route::get('artistas', [
	    'as'   => 'index',
	    'uses' => 'ArtistasController@index',
	]);
	Route::get('artistas/create', [
	    'as'   => 'create',
	    'uses' => 'ArtistasController@create',
	]);
	Route::get('artistas/destroy/{artista}', [
	    'as'   => 'destroy',
	    'uses' => 'ArtistasController@destroy',
	]);
	Route::post('artistas/{artista}', [
	    'as'   => 'update',
	    'uses' => 'ArtistasController@update',
	]);
	Route::get('artistas/{artista}/edit', [
	    'as'   => 'edit',
	    'uses' => 'ArtistasController@edit',
	]);
	Route::get('artistas/{artista}/show', [
	    'as'   => 'show',
	    'uses' => 'ArtistasController@show',
	]);

});

// Músicas
Route::name('musicas.')->group(function () {
	Route::post('musicas/store', [
	    'as'   => 'store',
	    'uses' => 'MusicasController@store',
	]);
	Route::get('musicas', [
	    'as'   => 'index',
	    'uses' => 'MusicasController@index',
	]);
	Route::get('musicas/artista/{artista}', [
	    'as'   => 'musicasArtista',
	    'uses' => 'MusicasController@musicasArtista',
	]);	
	Route::get('musicas/create', [
	    'as'   => 'create',
	    'uses' => 'MusicasController@create',
	]);
	Route::get('musicas/destroy/{musica}', [
	    'as'   => 'destroy',
	    'uses' => 'MusicasController@destroy',
	]);
	Route::post('musicas/{musica}', [
	    'as'   => 'update',
	    'uses' => 'MusicasController@update',
	]);
	Route::get('musicas/{musica}/edit', [
	    'as'   => 'edit',
	    'uses' => 'MusicasController@edit',
	]);
	Route::get('musicas/{musica}/show', [
	    'as'   => 'show',
	    'uses' => 'MusicasController@show',
	]);
});

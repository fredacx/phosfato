<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $artistas_id
 * @property mixed $nome
 * @property time $duracao
 */
class Musicas extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'musicas';

    /**
     * @var array
     */
    protected $fillable = ['artistas_id', 'nome', 'duracao'];

}
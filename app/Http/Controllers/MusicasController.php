<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\MusicasRequest;
use App\Musicas;
use App\Artistas;

class MusicasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function index(Request $request)
    {
        $musicas = Musicas::select('musicas.*', 'artistas.nome as artista')
            ->join('artistas','musicas.artistas_id','=', 'artistas.id')
            ->paginate(config('default.pagination.max'));
        $musicas->appends(Input::except('page'));

        return view('musicas.index',compact('musicas'));
    }

    public function musicasArtista(Artistas $artista)
    {
        $musicas = Musicas::select('musicas.*', 'artistas.nome as artista')
            ->join('artistas','musicas.artistas_id','=', 'artistas.id')
            ->where('musicas.artistas_id', '=', $artista->id)
            ->paginate(config('default.pagination.max'));
        $musicas->appends(Input::except('page'));

        return view('musicas.index',compact('musicas'));
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return view
     */
    public function create()
    {
        $artistas = Artistas::where('status', true)
               ->orderBy('nome', 'desc')
               ->get();

        return view('musicas.create', compact('artistas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MusicasRequest $request)
    {
        $musica = new Musicas();
        
        $musica->fill($request->all());
        $musica->save();

        $success = __('Música cadastrado com sucesso!');
        Session::flash('success',$success);

        return redirect()->route('musicas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Musicas  $musica
     * @return \Illuminate\Http\Response
     */
    public function show(Musicas $musica)
    {
        $musica = Musicas::select('musicas.*', 'artistas.nome as artista')
            ->join('artistas','musicas.artistas_id','=', 'artistas.id')
            ->where('musicas.id', '=', $musica->id)
            ->first();

        return view('musicas.show',compact('musica'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Musicas  $musica
     * @return \Illuminate\Http\Response
     */
    public function edit(Musicas $musica)
    {
        $artistas = Artistas::where('status', true)
               ->orderBy('nome', 'desc')
               ->get();        
        return view('musicas.edit',compact('musica', 'artistas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Musicas $musica
     * @return \Illuminate\Http\Response
     */
    public function update(MusicasRequest $request, Musicas $musica)
    {
        try {
            $musica->update($request->all());

        } catch (ModelNotFoundException $e) {
            $message = __('Houve um problema ao editar a música.');
            Session::flash('error', $message);

            return redirect()->route('musicas.edit');
        }

        $message = __('Música editado com sucesso.');
        Session::flash('success', $message);

        return redirect()->route('musicas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Musicas  $musicay
     * @return \Illuminate\Http\Response
     */
    public function destroy(Musicas $musica)
    {
        try {
            $musica->delete();

        } catch (ModelNotFoundException $e) {
            $message = __('Houve um problema ao remover a música.');
            Session::flash('error', $message);

            return redirect()->route('musicas.index');
        }

        $message = __('Música removido com sucesso.');
        Session::flash('success', $message);

        return redirect()->route('musicas.index');
    }
}

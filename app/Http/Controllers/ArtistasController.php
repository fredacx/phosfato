<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ArtistasRequest;
use App\Artistas;

class ArtistasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function index(Request $request)
    {
    	$generos = config('enums.generos');
        $artistas = Artistas::filterSearch()
        	->ativo()
            ->paginate(config('default.pagination.max'));

        $artistas->appends(Input::except('page'));

        return view('artistas.index',compact('artistas', 'generos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return view
     */
    public function create()
    {
        $generos = config('enums.generos');
        return view('artistas.create', compact('generos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArtistasRequest $request)
    {
        $artista = new Artistas();
        
        $artista->fill($request->all());
        $artista->save();

        $success = __('Artista cadastrado com sucesso!');
        Session::flash('success',$success);

        return redirect()->route('artistas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Artista  $artista
     * @return \Illuminate\Http\Response
     */
    public function show(Artistas $artista)
    {
        return view('artistas.show',compact('artista'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Artista  $artista
     * @return \Illuminate\Http\Response
     */
    public function edit(Artistas $artista)
    {
		$generos = config('enums.generos');
        return view('artistas.edit',compact('artista', 'generos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Artistas $artista
     * @return \Illuminate\Http\Response
     */
    public function update(ArtistasRequest $request, Artistas $artista)
    {
        try {
			$artista->update($request->all());

        } catch (ModelNotFoundException $e) {
            $message = __('Houve um problema ao editar o artista.');
            Session::flash('error', $message);

            return redirect()->route('artistas.edit');
        }

        $message = __('Artista editado com sucesso.');
        Session::flash('success', $message);

        return redirect()->route('artistas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Artistas  $artistay
     * @return \Illuminate\Http\Response
     */
	public function destroy(Artistas $artista)
    {
        try {
			$artista->status = false;
			$artista->save();

        } catch (ModelNotFoundException $e) {
            $message = __('Houve um problema ao remover o artista.');
            Session::flash('error', $message);

            return redirect()->route('artistas.index');
        }

        $message = __('Artista removido com sucesso.');
        Session::flash('success', $message);

        return redirect()->route('artistas.index');
    }
}

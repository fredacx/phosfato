<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArtistasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'nome' => 'required|max:50',
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'nome' => 'required|max:50',
                ];
            default: 
                break;
        }
    }

    public function messages()
    {
        return [
            'nome.required' => 'O nome deve ser preenchido.',
            'nome.max' => 'O nome deve conter um máximo de 50 caracteres.',
        ];
    }
}

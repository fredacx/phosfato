<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

/**
 * @property int $id
 * @property mixed $nome
 * @property mixed $genero
 */
class Artistas extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'artistas';

    /**
     * @var array
     */
    protected $fillable = ['nome', 'genero', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function musicas()
    {
        return $this->hasMany('Musicas');
    }

    public function scopeAtivo($query) {

        $filter=Request::only($this->sortable);
        $query->where('status','=',true);

        return $query;
    }      

    /**
     * Scope to search for screen filters. 
     */
    public function scopeFilterSearch($query) {


        $filter=Request::only($this->sortable);

        if(!empty($filter['nome'])){
            $query->where("nome","LIKE","%{$filter['nome']}%");
        }

        if(!empty($filter['genero'])){
            $query->where('genero','=',$filter['genero']);
        }

        return $query;
    }
}
